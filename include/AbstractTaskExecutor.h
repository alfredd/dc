#ifndef TASK_EXECUTOR_H
#define TASK_EXECUTOR_H
namespace alfredd 
{
    namespace dc
    {
        class TaskExecutor 
        {
            public:
                virtual void execute(AbstractTask task);
                virtual ~TaskExecutor() {}
        };
    }
}


#endif
