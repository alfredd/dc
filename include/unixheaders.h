/* 
 * File:   unixheaders.h
 * Author: alfredd
 *
 * Created on 19 June, 2013, 8:15 PM
 */

#ifndef UNIXHEADERS_H
#define	UNIXHEADERS_H


#define MAX_LEN 1024
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <signal.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#endif	/* UNIXHEADERS_H */

