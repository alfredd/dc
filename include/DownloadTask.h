#ifndef DOWNLOAD_TASK_H
#define DOWNLOAD_TASK_H
#include <string>
#include "AbstractTask.h"
#include "UriParser.h"

namespace alfredd
{
    namespace dc
    {
        class DownloadTask : public AbstractTask
        {
            public:
                DownloadTask(std::string url);
                virtual void run();
                virtual ~DownloadTask() {}
            private:
                UriParser parser;
        };
    }
}


#endif
