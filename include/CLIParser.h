/* 
 * File:   CLIParser.h
 * Author: alfredd
 *
 * Created on 30 September, 2013, 10:39 PM
 */

#ifndef CLIPARSER_H
#define	CLIPARSER_H
#include <string>

class CLIParser {
public:
    CLIParser();
    CLIParser(const CLIParser& orig);
    void ParseShortOpts(int argc, char* argv[]);
    inline std::string GetHostName(){return host;}
    inline std::string GetPort(){return protocol;}
    virtual std::string GetUsage();
    virtual ~CLIParser();
private:
    const char* options;
    std::string host;
    std::string protocol;

};

#endif	/* CLIPARSER_H */

