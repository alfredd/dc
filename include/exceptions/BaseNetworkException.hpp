#ifndef BASE_NETWORK_EXCEPTION__H
#define BASE_NETWORK_EXCEPTION__H
#include <exception>
#include <string>
#include <errno.h>
#include <string.h>
using namespace std;

namespace alfredd
{
    namespace exception
    {
        class BaseNetworkException : public exception
        {
            public:
                BaseNetworkException()
                {
                    cause= get_cause_from_errorno();
                    message = cause; 
                }

                BaseNetworkException(string message)
                {
                    this->message = message;
                    cause = get_cause_from_errorno();
                }

                virtual const char* what()
                {
                    return message.c_str();
                }
                virtual const char* why()
                {
                    return cause.c_str(); 
                }
            protected:
                virtual string get_cause_from_errorno()
                {
                    string s(strerror(errno));
                    return s;
                }
            private:
                string message;
                string cause;
        }
    }
}



#endif
