#ifndef FILEWRITER_H
#define FILEWRITER_H
#include <string>
#include "AbstractWriter.h"

namespace alfredd 
{
    namespace dc
    {
        class FileWriter : public AbstractWriter 
        {
            public:
                FileWriter (std::string file_name): filename(file_name) {}
                virtual int write(const std::string *data);
                virtual int write(const char* data, int len);
                virtual ~FileWriter() {}
            private:
                std::string filename;
                int file_descriptor;
        };
    }
}


#endif
