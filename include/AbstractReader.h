#ifndef ABSTRACTREADER_H
#define ABSTRACTREADER_H
#include <string>
namespace alfredd 
{
    namespace dc
    {
        class AbstractReader 
        {
            public:
                virtual std::string read() = 0;
                virtual int read(char* buffer) = 0;
                virtual ~AbstractReader() {}
        };
    }
}


#endif
