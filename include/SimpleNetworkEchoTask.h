#ifndef SIMPLE_NETWORK_ECHO_TASK__H
#define SIMPLE_NETWORK_ECHO_TASK__H

#include "AbstractTask.h"
#include "Socket.h"

namespace alfredd 
{
    namespace dc
    {
        class SimpleNetworkEchoTask : public AbstractTask
        {
            private:
                Socket *socket;
            public:
                SimpleNetworkEchoTask(Socket socket)
                {
                    this->socket = &socket;
                }
                virtual void run();
                virtual ~SimpleNetworkEchoTask()
                {
                    delete socket;
                }
        };
    }
}

#endif
