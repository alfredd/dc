/* 
 * File:   UtilityFunctions.h
 * Author: alfredd
 *
 * Created on 19 June, 2013, 8:25 PM
 */

#ifndef UTILITYFUNCTIONS_H
#define	UTILITYFUNCTIONS_H

#ifdef	__cplusplus
extern "C" {
#endif


    
void Assert(int status, int expected, const char *output);


#ifdef	__cplusplus
}
#endif

#endif	/* UTILITYFUNCTIONS_H */

