/* 
 * File:   Socket.h
 * Author: alfredd
 * Description:
 *      Socket Class Decleration. This class is built to be a base class for all types of Socket Connections.
 * The main goal being
 *
 * Created on 19 June, 2013, 8:17 PM
 */

#ifndef SOCKET_H
#define	SOCKET_H
#include "unixheaders.h"
#include <string>

class Socket 
{

    public:
        Socket(int socket, struct addrinfo* addr);

        Socket():sock(0), Host(""), Service(""){}
        Socket(std::string host, std::string service);
        virtual void connect();
        virtual int send (const std::string *buffer);
        virtual const std::string* receive();
        virtual ~Socket();
        const int get_socket_fd();


    protected:
        int sock;
        struct addrinfo hints, *res;
        std::string Host, Service;


};

#endif	/* MYSOCKET_H */

