#ifndef URI_PARSER__H
#define URI_PARSER__H
#include <string>

class UriParser
{
    private:
        std::string host;
        std::string protocol;
        std::string url;
    public:
        UriParser():host(""),protocol(""),url(""){}
        virtual void parse_url(const std::string& url);
        virtual bool is_valid(const std::string& url);
        const std::string get_host();
        const std::string get_protocol();
        const std::string get_url();
        const std::string get_uri();
        const std::string get_filename();
};
#endif
