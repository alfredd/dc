#ifndef RGET_H
#define RGET_H

#include <stdlib.h>
#include <string>

#define REMOTE_HOST "192.168.1.2"
#define PORT "4242"
#define HOME getenv("HOME")
#define DEFAULT_DIRECTORY "/Downloads/"
#define TIME_DISPLAY_FORMAT "%A, %d %b, %Y < %H:%M:%S >"

namespace alfredd { namespace dc {
    const std::string DOWNLOAD_DIRECTORY = 
                std::string(HOME) + std::string(DEFAULT_DIRECTORY);
} } 

enum DownloadType
{
    LOCAL,
    REMOTE
};

enum DownloadStatus
{
    STARTED,
    PAUSED,
    RESUME,
    GET_REMOTE,
    UNFINISHED,
    FILE_DATA,
    COMPLETED
};
#endif
