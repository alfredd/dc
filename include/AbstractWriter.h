#ifndef ABSTRACTWRITER_H
#define ABSTRACTWRITER_H

#include <string>

namespace alfredd 
{
    namespace dc
    {
        class AbstractWriter 
        {
            public:
                virtual int write(const std::string *data) = 0;
                virtual int write(const char* data, int len) = 0;
                virtual ~AbstractWriter() {}
        };
    }
}


#endif
