
#ifndef SERVER_SOCKET__H
#define SERVER_SOCKET__H
#include <string>
#include "Socket.h"
class ServerSocket : public Socket
{
    public:
        ServerSocket():Socket(){}
        ServerSocket(std::string service):Socket("0.0.0.0",service) {}
        virtual Socket* accept();
        virtual void bind();
        virtual void listen();
};
#endif
