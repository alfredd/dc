#ifndef ABSTRACT_TASK__H
#define ABSTRACT_TASK__H

namespace alfredd 
{
    namespace dc
    {
        class AbstractTask 
        {
            public:
                virtual void run() =0;
                virtual ~AbstractTask() {}
        };
    }
}

#endif
