#ifndef TASKDETAIL_HPP
#define TASKDETAIL_HPP

#include "rget.h"
#include <string>
#include <time.h>

using namespace std;

namespace alfredd 
{
    namespace dc
    {
        struct Connection
        {
            string host;
            string port; 
        }; 
        struct TaskDetail
        {
            string url;
            DownloadStatus status;
            DownloadType type;
            time_t start_time;
            time_t end_time;
            Connection request_initiator;
            Connection request_responder;
            string file_location;

            const string get_start_time()
            {
                return _get_time(start_time);
            }

            const string get_end_time()
            {
                return _get_time(end_time);
            }

            const string get_time_since_started()
            {
                return "Unimplemented as of now";
            }
             
            private:
                const string _get_time(time_t time_data)
                {
                    char time[128]={0};
                    tm *tm_data = localtime(&time_data);
                    strftime (time, 128, TIME_DISPLAY_FORMAT, tm_data);
                    return time;
                }

        };
    }
}
#endif
