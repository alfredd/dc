/* 
 * File:   Socket.cpp
 * Author: alfredd
 * Description: 
 *      Socket Class implementation.
 * 
 * Created on 19 June, 2013, 8:17 PM
 */

#include "Socket.h"
#include "CppHeaders.h"
#include "unixheaders.h"
#include "UtilityFunctions.h"
using namespace std;
Socket::Socket(int socket, struct addrinfo* addr)
{
    this->sock = socket;
    this->res = addr;
}


Socket::Socket(string host, string service)
{
    Host =host;
    Service = service;   
    bzero(&hints, sizeof(struct addrinfo));
    res=new (struct addrinfo);
    bzero(res, sizeof(struct addrinfo));   

    hints.ai_socktype=SOCK_STREAM;
    int status = getaddrinfo(Host.c_str(), Service.c_str(), &hints, &res);
    if (status !=0)
    {
        printf("Error: Could not resolve %s://%s :%s\n", Service.c_str(), Host.c_str(), gai_strerror(status));
        exit(status);

    }

    while(res)
    {
        if ((sock=socket(res->ai_family, res->ai_socktype, res->ai_protocol))>0)
            break;
        else
            res=res->ai_next;
    }
    if(sock<=0)
    {
        cout<<"Error in selecting socket"<<endl;
        exit(1);
    }
}


void Socket::connect() 
{

    struct sockaddr_storage *ss = new (struct sockaddr_storage);
    socklen_t len = sizeof(struct sockaddr_storage);
    cout<<"Base Connect"<<endl;
    Assert(::connect(sock, res->ai_addr, res->ai_addrlen),0,"Error in Connecting");
    cout<<"Connect Successful"<<endl;
    Assert(getpeername(sock, (struct sockaddr*) ss, &len),0,"Error in getpeername for socket");
    cout<<"Got peer name"<<endl;
    len = res->ai_addrlen;
    char* c = new char[len];
    bzero(c,len);

    const char* address = inet_ntop(ss->ss_family,ss, c, len);
    if (c!=NULL)
        cout<<"Connected to "<< address << ", Address " << c<<endl;
    else
        cout<<"error in getpeername of connected host"<<endl;
    
    delete [] c;

}

const string* Socket::receive()
{
    char buffer[MAX_LEN];
    bzero(buffer,MAX_LEN);
    int stat=recv(this->sock, buffer,MAX_LEN,0);
    const string* returnBuffer = new string (buffer);
//    return stat>0 ? returnBuffer : NULL;
    if(stat >0)
        return returnBuffer;
    perror("error in receiving data");
    return NULL;
}

int Socket::send(const std::string* buffer)
{
    if(buffer==NULL)
        return -1;
    int stat=::send(this->sock, buffer->c_str(),buffer->size(),0);
    return stat;
}


Socket::~Socket() 
{
    close(sock);
}


const int Socket::get_socket_fd()
{
    return this->sock;
}
