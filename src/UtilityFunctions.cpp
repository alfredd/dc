#include "UtilityFunctions.h"
#include "unixheaders.h"
#include <stdio.h>
#include <stdlib.h>
void Assert(int status, int expected, const char *output)
{
    if (status != expected)
    {
        printf("Error: %s\n",output,strerror(status));
        exit(status);
    }
        
}
