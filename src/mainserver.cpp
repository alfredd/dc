#include "ServerSocket.h"
#include "unixheaders.h"
#include <string>
#include <iostream>

using namespace std;
void sigchld_handler (int sig_number)
{

    if(sig_number != SIGCHLD)
        return;
    int stat;
    pid_t pid ; 
    while((pid=waitpid(-1,&stat,WNOHANG ))>0)
    {
        cout << "Process Terminated: "
            << pid 
            << ". Status: "
            << stat<<endl;
    }
}

void do_echo(Socket *client)
{
    const string* receivedBuffer=NULL; 
    do 
    {
        try 
        {   
            receivedBuffer =client->receive();
            cout<<"Received: receivedBuffer: " 
                << (receivedBuffer ==NULL ? " Error in receiving": *receivedBuffer) 
                <<endl;
            if( receivedBuffer ==NULL)
                break;
            client->send(receivedBuffer);
        } catch(string s)
        {
            cout<<"from the client "<<endl;
            cout<<s<<endl;
        }
    } while (receivedBuffer!=NULL);
}

int main()
{
    pid_t pid = 0;
    struct sigaction act,oact;
    act.sa_handler = sigchld_handler;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
#ifdef SA_RESTART 
    act.sa_flags |= SA_RESTART ;
#endif
    sigaction (SIGCHLD,&act,&oact);

    ServerSocket *server = new ServerSocket("10000");
    server->bind();
    server->listen();

    while(1)
    {
        Socket* client=NULL;      
        try 
        {
            client=server->accept();
        } catch (string e)
        {
            if ( errno == EINTR )
                continue;
            else 
            {
                cout << "Error in accept: "
                    << strerror (errno) 
                    << endl;
                exit(1);
            }
        }

        pid=fork();
        if(pid==0)
        {
            close(server->get_socket_fd()); 
            cout<<"Connection Received"<<endl;
            do_echo(client);
            exit(0);
        }
        cout<<"Closing client socket from server instance"<<endl; 
        close(client->get_socket_fd());
    }
}
