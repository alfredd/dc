#include <string>
#include <regex.h>
#include "UriParser.h"

using namespace std;
void UriParser::parse_url(const string& stringurl)
{
    int index = stringurl.find("://");
    protocol = stringurl.substr(0,index);
    index +=3;
    int nextindex=stringurl.find('/',index);
    nextindex-=index;
    host=stringurl.substr(index,nextindex);
    url=stringurl.substr(nextindex+index);
}

bool UriParser::is_valid(const string& stringurl)
{
    regex_t compiled;
    const char* REGEX_URL= "^(https?|ftp|file)://.+$";
    int status=regcomp(&compiled,REGEX_URL,REG_EXTENDED);
    if(status==0)
    {
        regmatch_t match;
        status = regexec(&compiled,stringurl.c_str(), 0, &match,REG_NOTEOL) ; 
        if (status==0)
            return true;
    }
    
    return false;
}

const std::string UriParser::get_host()
{
    return host;
}

const std::string UriParser::get_protocol()
{   
    return protocol;
}   

const std::string UriParser::get_url()
{   
    return url;
}   

const std::string UriParser::get_uri()
{   
    return protocol+"://"+host+"/"+url;
}   

const std::string UriParser::get_filename()
{
    int index = url.find_last_of("/");
    return url.substr(index+1, url.size()); 
}
