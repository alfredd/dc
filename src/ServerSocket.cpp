
#include "ServerSocket.h"
#include <string>
#include <iostream>

void  ServerSocket::bind()
{
    int status =::bind(sock,res->ai_addr,res->ai_addrlen);
    if(status!=0)
        throw std::string("Error in bind()-ing");
}

void ServerSocket::listen()
{
    int status = ::listen(sock,20);
    if(status!=0)
        throw std::string("Error in listen()-ing"); 
}

Socket* ServerSocket::accept()
{
    int client_socket;
    struct addrinfo *res = new (struct addrinfo);
    bzero(res, sizeof(struct addrinfo));
    client_socket = ::accept(sock, res->ai_addr,&res->ai_addrlen);
    if(client_socket<0)
    {
        perror("Error in accepting socket connection");
        throw std::string ("Error in accepting socket connection");
    }
    return client_socket > 0 ? new Socket(client_socket, res):NULL;
}
