/* 
 * File:   CLIParser.cpp
 * Author: alfredd
 * 
* Created on 30 September, 2013, 10:39 PM
 */
#include "unixheaders.h"
#include "CLIParser.h"
#include "CppHeaders.h"

CLIParser::CLIParser()
{
    host="";
    protocol="";
    options="h:p:";
}

CLIParser::CLIParser(const CLIParser& orig)
{
}

CLIParser::~CLIParser()
{
}
void CLIParser::ParseShortOpts(int argc, char* argv[])
{
    int c;
    int hflag=0;
    int pflag=0;
    while((c=getopt(argc, argv, options))!=-1)
    {
        switch(c)
        {
            case 'h':
                hflag=1;
                host=std::string(optarg);
                break;
            case 'p':
                pflag=1;
                protocol=std::string(optarg);
                break;
            case '?':
                throw std::string("Error in Usage");
                
        }
    }
    if(!hflag)
        throw std::string("Hostname is mandatory.");
    if(!pflag)
        throw std::string("Port is mandatory.");
}

std::string CLIParser::GetUsage ()
{
    std::string usage = " -h HOST -p PORT";
    usage += "\n HOST: Hostname or IP of the server system";
    usage += "\n PORT: Port address";
    return usage;
}
