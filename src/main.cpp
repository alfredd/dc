/* 
 * File:   main.cpp
 * Author: alfredd
 *
 * Created on 19 June, 2013, 8:09 PM
 */

#include "UriParser.h"
#include <cstdlib>
#include "CppHeaders.h"
#include "Socket.h"
#include "CLIParser.h"
#include "unixheaders.h"
#include <fstream>
using namespace std;




/*
 * 
 */
int main(int argc, char** argv) {

    string host;
    string port;
    if(argc !=2)
    {
        cerr<<"usage: " <<argv[0]<<" URL"<<endl;
        exit(1);
    }   
    UriParser parser;
    string url= string(argv[1]);
    //if(!parser.is_valid(url))
    //{
    //    cerr<<"Invalid URL: "<<url<<endl;
    //    exit(2);
    //}
    parser.parse_url(url);
    host  = parser.get_host();
    port  = parser.get_protocol();
    cout<<"Host: "<<host<<endl;
    cout<<"Port: "<<port<<endl; 
    cout<<"URi: " << parser.get_uri()<<endl;
    cout<<"URl: " << parser.get_url()<<endl;
    cout<<"File: "<< parser.get_filename()<<endl;
    Socket *s = new Socket(host,port);
    s->connect();
    string buff("GET "+url+" \r\r\n");
    s->send(&buff);
    ofstream outfile(parser.get_filename().c_str());
    if(!outfile)
    {
        cerr << "error in creating file: " << parser.get_filename()<<endl;
        exit(1);
    }
    const string* buffer;
    do
    {
        buffer = s->receive();

        if(buffer!=NULL)
            outfile << buffer->c_str();//[i];
    }while(buffer!=NULL);
    outfile.close();
    s->~Socket();
    return 0;
}

