#include <iostream>
#include <string>
#include "Socket.h"
#include "CLIParser.h"
#include <cstdlib>
#include "unixheaders.h"
using namespace std;

int main(int argc , char* argv[])
{
    CLIParser cliparser;
    try
    {
        cliparser.ParseShortOpts(argc, argv);
    } catch ( string e)
    {
        cout << "Exception: "<< e <<endl;
        cout << "Usage: "<< argv[0] << cliparser.GetUsage() <<endl;
        exit(1);
    }

    Socket s(cliparser.GetHostName(), cliparser.GetPort() );
    s.connect();
    string message;
    cout << "Type Message to send: ";
    cin >> message;
    
    s.send(&message);
    const string* buffer=s.receive();
    cout <<"Received: "<< buffer->c_str() <<endl;
    return 0;
}
